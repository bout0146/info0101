import java.util.Scanner;

class factorielle{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Entrez un nombre : ");
        int n = scanner.nextInt();
        System.out.println("La factorielle de " + n + " est : " + facto(n));

    }

    public static int facto(int a) {
        if (a < 0) {
            System.out.println("Le nombre doit être positif ou nul.");
        }

        int fact = 1; 
        for (int i = 1; i <= a; i++) {
            fact *= i; 
        }
        return fact;
    }
}