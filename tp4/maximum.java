import java.util.Scanner;

class maximum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Entrez le premier nombre réel : ");
        double premierNombre = scanner.nextDouble();
        
        System.out.print("Entrez le deuxième nombre réel : ");
        double deuxiemeNombre = scanner.nextDouble();
        
        System.out.println("Voici le maximum : " + max(premierNombre, deuxiemeNombre));
    }
    
    public static double max(double a, double b) {
        if (a > b) {
            return a;
        } else {
            return b;
        }
    }
}