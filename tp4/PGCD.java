import java.util.Scanner;

class PGCD {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a, b;

        do {
            System.out.print("Entrez le premier entier (a) : ");
            a = scanner.nextInt();
            System.out.print("Entrez le deuxième entier (b) : ");
            b = scanner.nextInt();
        } while (!estValide(a, b));

        int pgcd = calculerPGCD(a, b);
        System.out.println("Le PGCD de " + a + " et " + b + " est : " + pgcd);
    }

    public static int calculerPGCD(int a, int b) {
        while (b != 0) {
            int r = a % b;
            a = b;
            b = r;
        }
        return a;
    }

    public static boolean estValide(int a, int b) {
        if (a < 0 || b < 0) {
            System.out.println("Les valeurs doivent être des entiers positifs.");
            return false;
        }
        if (b > a) {
            System.out.println("La valeur de b ne peut pas être supérieure à a.");
            return false;
        }
        return true;
    }
}
