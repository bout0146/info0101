import java.util.Scanner;

class maximum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Entrez le nombre d'éléments (n) : ");
        int n = scanner.nextInt();
        
        if (n <= 0) {
            System.out.println("Veuillez entrer un nombre entier positif.");
            return;
        }
        
        System.out.print("Entrez le premier nombre réel : ");
        double premierNombre = scanner.nextDouble();
        double maxi = premierNombre;
        
        for (int i = 1; i < n; i++) {
            System.out.print("Entrez le nombre réel " + (i + 1) + " : ");
            double nombre = scanner.nextDouble();
            maxi = max(maxi, nombre);
        }
		
        System.out.println("Voici le maximum : " + maxi);
    }
    public static double max(double a, double b) {
        if (a > b) {
            return a;
        } else {
            return b;
        }
    }
}