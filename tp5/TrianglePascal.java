import java.util.Scanner;

class TrianglePascal {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Entrez un nombre n : ");
        int n = scanner.nextInt();
        
        afficherTriangle(n);
        long maxCoeff = coeffMaximum(n);
        System.out.println("Le plus grand coefficient binomial pour la ligne " + n + " est : " + maxCoeff);
    }
    
    public static long facto(int a) {
        if (a < 0) {
            System.out.println("Le nombre doit être positif ou nul.");
            return -1;
        }

        long fact = 1; 
        for (int i = 1; i <= a; i++) {
            fact *= i; 
        }
        return fact;
    }

    public static long coeffBinomial(int c, int d) {
        if (c < 0 || d < 0 || d > c) {
            System.out.println("Les nombres doivent être positifs ou nuls, et d ne doit pas être supérieur à c.");
            return -1;
        }

        long coeff = facto(c) / (facto(d) * facto(c - d));
        return coeff;
    }
    
    public static void afficherLigne(int e, int nombreDeChiffres) {
        if (e < 0) {
            System.out.println("Le nombre doit être positif ou nul.");
            return;
        }
        
        for (int i = 0; i <= e; i++) {
            long coeff = coeffBinomial(e, i);
            afficherValeur((int) coeff, nombreDeChiffres);
            if (i < e) {
                afficherEspace(nombreDeChiffres); 
            }
        }
        System.out.println(); 
    }

    public static void afficherTriangle(int f) {
        if (f < 0) {
            System.out.println("Le nombre doit être positif ou nul.");
            return;
        }

        long maxCoeff = coeffMaximum(f);
        int nombreDeChiffres = nbDigits(maxCoeff);
        
        for (int i = 0; i <= f; i++) {
            int espaces = (f - i) * (nombreDeChiffres + 1) / 2;
            afficherEspace(espaces);
            afficherLigne(i, nombreDeChiffres);
        }
        System.out.println();
    }

    public static int nbDigits(long n) {
        if (n == 0) {
            return 1;
        }
        
        n = Math.abs(n);
        
        int count = 0;
        while (n > 0) {
            n /= 10;
            count++;
        }
        
        return count;
    }
    
    public static long coeffMaximum(int c) {
        long maxCoeff = 0;
        for (int i = 0; i <= c; i++) {
            long coeff = coeffBinomial(c, i);
            if (coeff > maxCoeff) {
                maxCoeff = coeff;
            }
        }
        return maxCoeff;
    }

    public static void afficherEspace(int n) {
        for (int i = 0; i < n; i++) {
            System.out.print(" ");
        }
    }

    public static void afficherValeur(int nombre, int nombreDeChiffres) {
        int digitsDansNombre = nbDigits(nombre);
        int espaces = nombreDeChiffres - digitsDansNombre;
        
        for (int i = 0; i < espaces; i++) {
            System.out.print(" ");
        }
        System.out.print(nombre);
    }
	public static long ligneMax(int seuil) {
		System.out.println("Donnez un seuil");
		seuil = Scanner.nextInt();
		if (coeffMaximum <= seuil) {
			
		}
	}
	public static long puissance2(int) {}
	public static long sommeCoefficient(int) {}
	public static long nbLapins(int) {}
	public static long fibonacci(int){}
}
